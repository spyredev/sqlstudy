
select * from emp;

use angajati;


select * from clienti;

select * from produse;

describe clienti;

select * from produse where idc is not null;
select * from produse where idc is null;

update produse set idc = 200 where id = 1;

select * from produse;
select * from clienti;

select id, nume, pret from produse;
select produse.id, produse.nume, produse.pret from produse;

select produse.nume, produse.pret, clienti.nume_client from produse, clienti where clienti.id = produse.idc;

select nume, pret, nume_client from produse, clienti where clienti.id = produse.idc;

select id, nume, pret, nume_client from produse, clienti where clienti.id = produse.idc;

select produse.id, produse.nume, produse.pret, clienti.nume_client from produse, clienti where clienti.id = produse.idc;

select p.id, c.id, p.nume, p.pret, c.nume_client from produse p, clienti c where c.id = p.idc;

select p.id, p.nume from produse p;


select p.id as 'ID produs', c.id as 'ID CLIENTI', p.nume, p.pret, c.nume_client from produse p, clienti c where c.id = p.idc;

select p.id, c.id, p.nume, p.pret, c.nume_client from produse p inner join clienti c on c.id = p.idc;

select * from produse;
select * from clienti;

update produse set idc = 2 where nume = 'laptop';
update produse set idc = 2 where id = 1;

select p.id, p.nume, p.pret, c.nume_client from produse p left join clienti c on c.id = p.idc; -- toate produsele, chiar daca nu au clienti asociati

select p.id, p.nume, p.pret, c.nume_client from produse p left join clienti c on c.id = p.idc;


