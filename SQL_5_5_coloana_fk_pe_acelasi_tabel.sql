select * from emp;
select * from dept;

select e1.ename, e1.job from emp e1 where e1.mgr = 7839;
select e1.ename, e1.job, e2.ename, e2.job from emp e1 inner join emp e2 on e1.mgr = e2.empno;


create table categorii(id int primary key auto_increment, nume varchar(240) not null);
alter table categorii add column parent_category int;
select * from categorii;

select * from categorii where nume like '%metal';
update categorii set parent_category = 1 where id >=2;

insert into categorii(nume) values ('metal'), ('thrash metal'), ('death metal'), ('math metal');
insert into categorii(nume) values ('jazz'), ('bebop');
insert into categorii(nume) values ('classical');

select * from categorii;
update categorii set parent_category = 3 where id = 8;

insert into categorii(nume) values('melodic death');

select * from categorii where parent_category = 1;

select sub.nume, par.nume from categorii sub inner join categorii par on sub.parent_category = par.id;

select * from categorii;
select * from categorii_meta;

insert into categorii(nume) values ('manele');
insert into categorii(nume) values ('taraneasca');
insert into categorii(nume) values ('folk');
insert into categorii(nume) values ('rap');
insert into categorii(nume) values ('hip-hop');
create table categorii_meta(id int primary key auto_increment, nume_cat varchar(240), data_adaugare date);
alter table categorii_meta add column introdusa_de varchar(240);
select now();
select user();

drop trigger log_categorii;

delimiter $$
CREATE TRIGGER log_categorii
AFTER INSERT
   ON categorii FOR EACH ROW
BEGIN

	insert into categorii_meta(nume_cat, data_adaugare, introdusa_de) values(new.nume, now(), user());
END;
$$
delimiter ;