select * from emp;
select * from dept;

drop table produse;
create table produse(id int primary key auto_increment, numeprodus varchar(240) not null, pret double);
select * from produse;
insert into produse(numeprodus, pret) values('laptop', 3333.22), ('desktop', 1500), ('tableta', 1000);

drop table inventar;
create table inventar(id int primary key auto_increment, id_produs int, cantitate int, foreign key(id_produs) references produse(id));

select * from inventar;
insert into inventar(id_produs, cantitate) values(1, 23);

insert into inventar(id_produs, cantitate) values(1, 10);
update inventar set cantitate = cantitate + 10 where id_produs = 1;
insert into inventar(id_produs, cantitate) values(2, 15);
insert into inventar(id_produs, cantitate) values(3, 70);

select p.numeprodus, i.cantitate, i.id_produs from produse p inner join inventar i on i.id_produs = p.id ;
select p.numeprodus, sum(i.cantitate) from produse p inner join inventar i on i.id_produs = p.id group by id_produs;

select id_produs, cantitate from inventar;
select id_produs, sum(cantitate) from inventar group by id_produs;


drop table inventar;
create table inventar(id_produs int primary key, cantitate int, foreign key(id_produs) references produse(id));

select * from inventar;

select p.numeprodus, i.cantitate from produse p inner join inventar i on i.id_produs = p.id;

select * from produse;
create table orders(id_order int primary key auto_increment, data_order datetime, id_produs int, cantitate int, foreign key(id_produs) references produse(id));

select * from orders;
insert into orders(data_order, id_produs, cantitate) values(now(), 1, 7);
insert into orders(data_order, id_produs, cantitate) values(now(), 1, 3);
insert into orders(data_order, id_produs, cantitate) values(now(), 2, 11);

select * from inventar;

drop trigger scadere_inventar;
delimiter $$
create trigger scadere_inventar
after insert  on orders
	for each row
begin    
    update inventar i set i.cantitate = i.cantitate - new.cantitate where i.id_produs = new.id_produs;
    
end;
$$
delimiter ;
