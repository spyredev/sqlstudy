
select * from angajati.produse;

create database magazin1;

use magazin1;

create table magazin1.clienti like angajati.clienti;

-- copiere tabel pas 1 - copiere strucura
create table produse like angajati.produse;

select * from produse;

insert into produse(id, nume, descriere, pret, idc) values(10000, 'procesor i5', 'procesor foarte fain', 300, 4703);

alter table produse drop column idc;
-- insert into produse(

-- copiere tabel pas 2 - copiere informatii
insert into produse(nume, descriere, pret) select nume, descriere, pret from angajati.produse;

select * from produse;
select * from clienti;

insert into clienti select * from angajati.clienti;

-- un client poate cumpara MAI MULTE produse
-- un produs poate fi cumparat de MAI MULTI CLIENTI

-- many to many problematic (din cauza id-ului, se pot repeta 'combo'-urile id_produs, id_client)
create table clienti_si_produse (id int primary key auto_increment, id_produs int, id_client int);

alter table clienti_si_produse add constraint legatura_cu_produse foreign key(id_produs) references produse(id);
alter table clienti_si_produse add constraint legatura_cu_clienti foreign key(id_client) references clienti(id);

select * from produse;
select * from clienti;

insert into clienti_si_produse(id_produs, id_client) values(10001, 1);
insert into clienti_si_produse(id_produs, id_client) values(10002, 1);
insert into clienti_si_produse(id_produs, id_client) values(10003, 2);
insert into clienti_si_produse(id_produs, id_client) values(10003, 3);

select * from clienti_si_produse;

delete from clienti_si_produse where id = 5;

-- drop table clienti_si_produse;

-- many to many ok
create table clienti_si_produse (id_produs int, id_client int, 
										primary key(id_produs, id_client),
										foreign key(id_produs) references produse(id),
										foreign key(id_client) references clienti(id));

select count(id_produs) from clienti_si_produse where id_produs = 10003; 
select * from clienti_si_produse;

alter table clienti_si_produse add column cantitate int default 0;

select * from produse;

select * from clienti_si_produse;
select * from clienti;


update clienti_si_produse set cantitate = 733 where id_produs = 10001 and id_client = 1;


-- selectam toate produsele dar cu numele clientului la care au fost vandute atata timp cat cantitatea este > 200
-- NOU 20.04
-- produsele vandute mai mult de 200
select p.nume, p.pret, csp.cantitate from produse p, clienti_si_produse csp where p.id = csp.id_produs;
select p.nume, p.pret, csp.cantitate, c.nume_client from produse p inner join clienti_si_produse csp on p.id = csp.id_produs
													inner join clienti c on c.id = csp.id_client where csp.cantitate > 200;
                                                    
select p.*, c.nume_client, csp.cantitate from produse p, clienti c, clienti_si_produse csp
											where p.id = csp.id_produs and c.id = csp.id_client and csp.cantitate > 200;

select * from clienti;
insert into clienti(nume_client) values ('Jimmy');

select * from clienti_si_produse;

select c.*, csp.id_produs, csp.cantitate from clienti c left join clienti_si_produse csp on c.id = csp.id_client;


