select * from emp;

select sal from emp;

select sal from emp order by sal desc limit 1;

select max(sal) from emp;
select min(sal) from emp;
select avg(sal) from emp;

-- aflam toti angajatii cu salariu sub medie
select avg(sal) from emp;
select * from emp where sal < 2022;

select * from emp where sal < (select avg(sal) from emp); -- subquery, subselect, select in select

select * from dept;

select * from emp;
-- selectam toti angajatii (*) din departamentele care au bugetul peste 9000

select * from dept where buget > 9000;
select * from emp where deptno in (10, 40);


select deptno from dept where buget > 9000;
select * from emp where deptno in (select deptno from dept where buget > 9000);

select * from dept;
select * from emp;

select max(sal) from emp;
select min(sal) from emp;
select avg(sal) from emp;

select sum(sal) from emp;
select count(*) from emp;
select * from emp;

select sum(sal), deptno from emp where deptno = 10;
select sum(sal), deptno from emp where deptno = 20;
select sum(sal), deptno from emp where deptno = 30;
select sum(sal), deptno from emp where deptno = 40;

select sum(sal), deptno from emp where deptno = 10
union all
select sum(sal), deptno from emp where deptno = 20
union all
select sum(sal), deptno from emp where deptno = 30
union all
select sum(sal), deptno from emp where deptno = 40;

select * from emp;
select sum(sal), deptno from emp group by deptno;
select sum(sal) from emp;
select sum(sal) from emp group by deptno;

select count(*) from emp;
select count(*) from emp group by deptno; -- nu este destul de explicit
select count(*), deptno from emp group by deptno having count(*) > 2;

-- toate informatiile angajatilor de la departamentele care au cel putin 3 de angajati

select * from emp where deptno in(20, 30);
select * from emp where deptno in(select deptno from emp group by deptno having count(*) > 2);

